﻿using System;
using System.Collections.Generic;
using NHibernate;

namespace Events.Models
{
    public class DAO
    {
        private static DAO instance = null;

        public static DAO getInstance()
        {
            if (instance == null)
            {
                instance = new DAO();
            }
            return instance;
        }

        public IList<Event> getAllEvents()
        {
            ISession session = NHibernateHelper.OpenSession();
            IList<Event> events = session.QueryOver<Event>().List<Event>();
            return events;
        }

        public Event getEventbyId(int id)
        {
            ISession session = NHibernateHelper.OpenSession();
            Event e = session.Get<Event>((System.Int64)id);
            return e;
        }
        public IList<Event> getEventsbyDate(DateTime date)
        {
            ISession session = NHibernateHelper.OpenSession();
            IList<Event> events = session.QueryOver<Event>().Where(m => m.Date == date).List<Event>();
            return events;
        }

        public IList<Event> getEventsbyDateRange(DateTime startDate,DateTime endDate)
        {
            ISession session = NHibernateHelper.OpenSession();
            IList<Event> events = session.QueryOver<Event>().Where(m => m.Date <= endDate && m.Date >= startDate).List<Event>();
            return events;
        }

        public bool deleteEvent(int id)
        {
            ISession session = NHibernateHelper.OpenSession();
            ITransaction transaction = session.BeginTransaction();
            try
            {
                
                session.Delete(string.Format("from Event where id = {0}", id));
                transaction.Commit();
                return true;
            }catch(Exception ex)
            {
                transaction.Rollback();
                return false;
            }
        }

        public IList<Event> searchEvents(string text)
        {
            ISession session = NHibernateHelper.OpenSession();
            IList<Event> events = session.QueryOver<Event>().WhereRestrictionOn(x => x.Content).IsLike(string.Format("%{0}%",text)).List<Event>();
            return events;
        }

        public bool updateEvent(int id,string content)
        {
            ISession session = NHibernateHelper.OpenSession();
            ITransaction transaction = session.BeginTransaction();
            try
            {
                Event e = session.Get<Event>((System.Int64)id);
                e.Content = content;
                session.Save(e);
                transaction.Commit();
                return true;
            }
            catch(Exception ex)
            {
                transaction.Rollback();
                return false;
            }
        }

        public bool addEvent(Event e)
        {
            ISession session = NHibernateHelper.OpenSession();
            ITransaction transaction = session.BeginTransaction();
            try
            {
                session.Save(e);
                transaction.Commit();
                return true;
            }
            catch (Exception ex)
            {
                transaction.Rollback();
                return false;
            }
        }
    }
}