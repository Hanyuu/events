﻿using System;
using FluentNHibernate.Mapping;
namespace Events.Models
{
	public class Event
	{
		private long id;
		private DateTime date;
		private string content;
        private string members;

        public virtual long ID
		{
			get { return id; }
			protected set { id = value; }
		}

		public virtual string Content
		{
			get { return content; }
			set { content = value; }
		}

		public virtual DateTime Date
		{
			get { return date; }
			set { date = value; }
		}

        public virtual string Members
        {
            get { return members; }
            set { members = value; }
        }
        public Event()
        {

        }
		public Event(string content, DateTime date,string members)
		{
            this.content = content;
            this.date = date;
            this.members = members;
		}
	}

    public class EventMap : ClassMap<Event>
    {
        public EventMap()
        {
            Id(x => x.ID);
            Map(x => x.Date);
            Map(x => x.Content);
            Map(x => x.Members);

        }
    }
}