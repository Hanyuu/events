﻿$(document).ready(function () {
    let add = $('#add-event')
        , showEventAddModalWindow = $('#show-modal')
        , eventDate = $('#event-date')
        , eventContent = $('#event-content')
        , eventMembers = $('#event-members')
        , eventAddError = $('#event-add-alert')
        , addEventWindow = $('#add-event-window')
        , currentDate = $('#current-date')
        , mounthBack = $('#mounth-back')
        , mounthForward = $('#mounth-forward')
        , searchInput = $('#search')
        , refresh = $('#refresh')
        , editEventWindow = $('#edit-event-window')
        , datesOfWeekTable = $('#dates-of-week-table')
        , showEventWindow = $('#show-event-window')
        , date = new Date()
        , months = ['Январь', 'Февраль', 'Март', 'Апрель', 'Май', 'Июнь', 'Июль', 'Август', 'Сентябрь', 'Октябрь', 'Ноябрь', 'Декабрь']
        , provider = new Provider();

    let loadEvents = async function () {
        let events = await provider.loadEvents(date.getFullYear(), date.getMonth());

        for (let event of events) {
            event.Date = new Date(event.Date);
            let eventDay = datesOfWeekTable.find('[data-week-day=' + event.Date.getDate() + ']');
            eventDay.attr('data-event', event.ID).css('background-color', 'yellow');
        }    
        
    };

    let setDate = function (newDate) {
        currentDate.text(months[newDate ? newDate.getMonth() : date.getMonth()] + ' ' + (newDate ? newDate.getFullYear() : date.getFullYear()));
        fillDatesTable();
        loadEvents();
    }

    let editEventById = async function (id) {
        let data = await provider.getEventbyId(id);
        let eventData = editEventWindow.find('#event-date');
        let eventMembers = editEventWindow.find('#event-members');
        let eventContent = editEventWindow.find('#event-content');
        eventData.text(data.Date.split('T')[0]);
        eventMembers.text(data.Members);
        eventContent.val(data.Content);
        editEventWindow.find('#delete-event').click(async function () {
            let result = await provider.deleteEvent(id);
            if (result) {
                setDate(date);
                editEventWindow.modal('hide');
            }
        });
        editEventWindow.find('#update-event').click(async function () {
            let result = await provider.updateEvent(id, {
                'data': eventData.text(),
                'members': eventMembers.text(),
                'content': eventContent.val()
            });
            if (result) {
                editEventWindow.modal('hide');
            }
        });
        editEventWindow.modal('show');
    }
    let fillDatesTable = async function () {
        datesOfWeekTable.find('.day-of-week').remove();
        let newDate = new Date(date.getFullYear(), date.getMonth() + 1, 0)
        currentLine = 0;
        for (let i of [...Array(newDate.getDate()).keys()]) {
            if (i % 7 == 0) {
                currentLine++;
            }
            let el = $.tmpl($('#day-of-week-tmpl'), { dateNumber: (i + 1) });

            el.appendTo($('#week-line-' + currentLine));
            el.click(async function () {
                if (el.attr('data-event')) {
                    await editEventById(el.attr('data-event'));
                } else {
                    showAddEventWindow(undefined, el.attr('data-week-day'));
                }
            });
        }
    }

    let showAddEventWindow = function (e, dayOfMounth) {
        let eventdate = date;
        if (dayOfMounth) {
            eventdate = new Date(eventdate.setDate(dayOfMounth));
        }
        eventContent.val('');
        eventMembers.val('');
        eventDate.text(eventdate.toISOString().split('T')[0]);
        eventAddError.hide();
        addEventWindow.modal('show');
    }

    let showEventById = async function (id) {
        let data = await provider.getEventbyId(id);     
         showEventWindow.find('#event-date').text(data.Date.split('T')[0]);
         showEventWindow.find('#event-members').text(data.Members);
         showEventWindow.find('#event-content').text(data.Content);
         showEventWindow.modal('show');  
    }

    mounthBack.click(function () {
        let mounth = date.getMonth() - 1;
        if (mounth == -1) {
            date = new Date(date.getFullYear() - 1, 11, 1,12)
        } else {
            date = new Date(date.getFullYear(), mounth, 1,12)
        }
        setDate();
    });

    mounthForward.click(function () {
        let mounth = date.getMonth() + 1;

        if (mounth == 12) {
            date = new Date(date.getFullYear() + 1, 0, 1,12)
        } else {
            date = new Date(date.getFullYear(), mounth, 1,12)

        }
        setDate();
    });

    showEventAddModalWindow.click(showAddEventWindow);
    
    add.click(async function (e) {

        if (eventDate.text().length == 0)
        {
            eventAddError.text('Не указана дата события').show();
            return;
        }
        if (eventContent.val().length == 0)
        {
            eventAddError.text('Не указано описание события').show();
            return;
        }
        if (eventMembers.val().length == 0) {
            eventAddError.text('Укажите участников события').show();
            return;
        }
        let result = await provider.addEvent({
            'date': eventDate.text(),
            'content': eventContent.val(),
            'members': eventMembers.val().replace('\n', ',')
        });
        if (result) {
            addEventWindow.modal('hide');
            setDate(date);
        } else {
            eventAddError.text('Не удалось сохранить событие').show();
        }
        
    });

    searchInput.select2({
        allowClear: true,
        placeholder: 'Поиск',
        ajax: {
            url: "/api/events/",
            dataType: 'json',
            delay: 250,
            data: function (params) {
                return {
                    searchText: params.term,
                };
            },
            processResults: function (data, params) {
                let r = [];                
                for (let item of data) {

                    r.push({ 'id': item.ID, 'text':item.Content});
                }
                return {
                    results: r
                };
            },
            cache: true
        }
    });

    searchInput.change(function (e) {
        showEventById($(this).val());
    });

    refresh.click(function () {
        setDate();
    });
  
    setDate();
});