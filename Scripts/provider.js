﻿var Provider = function () {
    function Provider() { };

    Provider.prototype.addEvent = async function (data)
    {
       return  $.ajax({
            type: 'POST',
            url: '/api/events/',
            data: data
        })
    }

    Provider.prototype.updateEvent = async function (id,data) {
        return $.ajax({
            type: 'PUT',
            url: '/api/events/' + id,
            data: data
        })
    }

    Provider.prototype.deleteEvent = async function (id) {
        return $.ajax({
            type: 'DELETE',
            url: '/api/events/' + id,
        })
    }

    Provider.prototype.loadEvents = async function (year, month) {
        //I know, this bad, but it very easy,
        month = (month + 1)
        let startDate = year + '-' + month + '-' + '1' 
            , daysCount = new Date(year, month, 0).getDate()
            , endDate = year + '-' + month + '-' + daysCount 
        return $.ajax({
            type: 'GET',
            url: '/api/events/?startDate=' + startDate + '&endDate=' + endDate,
        })

    }
    Provider.prototype.getEventbyId = async function (id) {
        return $.ajax({
            type: 'GET',
            url: '/api/events/'+id,
        })

    }

    return Provider;
}()