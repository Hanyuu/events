﻿using System;
using System.Collections.Generic;
using System.Web.Http;
using Events.Models;
namespace Events.Controllers
{
    public class EventsController : ApiController
    {
        // GET api/events
        public IList<Event> Get([FromUri] string startDate=null, [FromUri] string endDate = null,[FromUri] string searchText=null)
        {
            if (startDate == null && endDate == null && searchText == null)
            {
                return DAO.getInstance().getAllEvents();
            }
            if(startDate != null && endDate != null)
            {
                DateTime sDate = DateTime.Parse(startDate);
                DateTime eDate = DateTime.Parse(endDate);
                return DAO.getInstance().getEventsbyDateRange(sDate, eDate);
            }
            if(searchText != null)
            {
                return DAO.getInstance().searchEvents(searchText);
            }
            throw new Exception("Wrong params");
        }

        // GET api/events/5
        public Event Get(int id)
        {
            return DAO.getInstance().getEventbyId(id);
        }

        // POST api/events
        public bool Post(Event e)
        {
            if (ModelState.IsValid)
            {
                return DAO.getInstance().addEvent(e);
            }
            return false;
        }

        // PUT api/events/5
        public bool Put(int id, Event e)
        {
            return DAO.getInstance().updateEvent(id, e.Content);
        }

        // DELETE api/events/5
        public bool Delete(int id)
        {
            return DAO.getInstance().deleteEvent(id);
        }
    }
}
